﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Creinmus Travel</title>
    <meta name="description" content="Spirit8 is a Digital agency one page template built based on bootstrap framework. This template is design by Robert Berki and coded by Jenn Pereira. It is simple, mobile responsive, perfect for portfolio and agency websites. Get this for free exclusively at ThemeForces.com">
    <meta name="keywords" content="bootstrap theme, portfolio template, digital agency, onepage, mobile responsive, spirit8, free website, free theme, themeforces themes, themeforces wordpress themes, themeforces bootstrap theme">
    <meta name="author" content="ThemeForces.com">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">
	

    <!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" type="text/css" href="fontsRedes.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <script type="text/javascript">
    
  </script>

  </head>
  <body id="tf-about" >
    <!-- Título
    ==========================================-->
   
        
	   <div id="tf-img" class="col-lg-offset-3 col-md-offset-4 col-lg-6 col-md-2 col-md-offset-4 col-lg-offset-3" align="center">
                <br>
            <img class="imagen img-responsive"  src="img/logo.png" align="center"></a>

    </div>
  <div class="container"><p></p></div>
    
	
	<!-- imagen y registro 
    ==========================================-->
	
<div class="text-center" align="center">
     <em><p class="intro"><strong>Metodología de ideación, desarrollo y consolidación de proyectos</strong></p></em>
    <div id="tf-cont" class="container" >
       <div class="box" style=" ;background-color:rgba(255,255,255,0.7);padding:15px 0">

            
                <div class="row">
                   <div class="col-md-6">
                    <img src="img/02.jpg" class="img-responsive">
                   </div>
                  <div class="col-md-6">
                       <div class="about-text">
                           <div class="section-title">
                               <h2> <strong>Regístrate</strong></h2> 
                               <div  class="line" align="right">
                                   <hr>
                                </div>
                                 <div class="clearfix" align="right"></div>   
                           
                                 <h5> <p align="justify">Déjanos tus datos y sé el primero en enterarte del lanzamiento de este producto y obtén beneficios especiales por ser un cliente pionero.</p><h5>
                                  
                             </div>
                                <form class="form-group" action="registrar-usuario.php" method="POST">
                                     <br><p> 
                                               <div class="form-group" >
                                               <h6><label for="exampleInputEmail1">Nombre</label></h6>
                                            <input type="name" name="nombre" class="form-control" id="nombre" placeholder="Ingresa tu nombre">
                                           </div>
                                         </p> 
                                         <p>  <div class="form-group">
                                               <h6><label for="exampleInputEmail1">Correo</label></h6>
                                              <input name="correo" type="email" class="form-control" id="correo" placeholder="Ingresa tu correo">
                                               </div>
                                         </p>
                                        <div class="form-group" align="left">
                                          <input type="checkbox" name="condiciones" ><label for="condiciones">Acepta las <a href="#">condiciones</a></label></input>
                                        </div>

                                      </br>
                                       <button id="tf-but" type="submit" class="btn tf-btn btn-default">Enviar</button>
                                </form>
                      </div>
                  </div>                
              </div>
           
        </div>
    </div>
    </br>

        <h3> <em><p>Estamos en construcción, espéranos pronto</p></em><h3>

</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Terminos y condiciones</h4>
      </div>
      <div class="modal-body">
        <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Redes sociales Laterales
    ==========================================-->
    <div class="social2">   
         <ul>
             <li><a href="http://www." target="_blank" class="icon-facebook2" ></a></li>
             <li><a href="http://www." target="_blank" class="icon-twitter"></a></li>
             <li><a href="http://www." target="_blank" class="icon-google-plus"></a></li>
             <li><a href="mailto:altair7812@gmail.com" target="_blank" class="icon-mail"></a></li>
             
    
        </ul>
    </div>
	

	<!-- Footer
    ==========================================-->
    
    <nav id="footer">
        <div class="container">
   
            <div class="text-center">
                <p>ALL RIGHTS RESERVED. COPYRIGHT © 2017.</p><p><a href="">Términos y condiciones</a></p> <p><a href="" ></a></p>
                <input name="terminos" type="checkbox" /><button type="button" class="" data-toggle="modal" data-target="#myModal">Política de privacidad</button>
            </div>
      
           <p> <div class="pull-right fnav">
                <ul class="footer-social">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div></p>
        </div>
    </nav>
    


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/SmoothScroll.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>

    <script src="js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>