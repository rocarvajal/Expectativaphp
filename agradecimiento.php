﻿
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Creinmus Travel</title>
    <meta name="description" content="Spirit8 is a Digital agency one page template built based on bootstrap framework. This template is design by Robert Berki and coded by Jenn Pereira. It is simple, mobile responsive, perfect for portfolio and agency websites. Get this for free exclusively at ThemeForces.com">
    <meta name="keywords" content="bootstrap theme, portfolio template, digital agency, onepage, mobile responsive, spirit8, free website, free theme, themeforces themes, themeforces wordpress themes, themeforces bootstrap theme">
    <meta name="author" content="ThemeForces.com">
    
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">
	

    <!-- Slider
    ================================================== -->
    <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="fontsRedes.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,700,300,600,800,400' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="http://platform.twitter.com/widgets.js" type="text/javascript"> </script>


  </head>
  <body id="tf-about" >

    <!-- Título
    ==========================================-->
   
        
	   <div id="tf-img" class="col-lg-offset-4 col-md-offset-4 col-lg-4 col-md-2 col-md-offset-4 col-lg-offset-4" align="center">
                <br>
            <a><img class="imagen img-responsive"  src="img/logo.png" align="center"></a>

    </div>
  <div class="container"><p></p>

  </div>
    
	
	<!-- imagen y registro 
    ==========================================-->
	
<div class="text-center" align="center">
     <em><p class="intro"><strong></strong></p></em>
    <div id="tf-cont" class="container" >
       <div class="box" style=" ;background-color:rgba(255,255,255,0.7);padding:15px 0">

            
              <div class="row">
                   
                  <div class="col-md-12">
                       <div class="about-text">
                           <div class="section-title">
                               <p><h2> <strong>Muchas gracias por compartir tus datos</strong></h2> </p>
                              <div  class="line" align="right">
                                   <hr>
                              </div>
                              <div class="clearfix" align="right">
                                
                              </div>   
                           
                                 <h5> <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu</p><h5>
                                 <a href="/Paginasexpectativa/indexCreinmus.php"><button id="tf-but" type="submit" class="btn tf-btn btn-default">volver</button></a>
                                  
                            </div>
                                
                      </div>
                  </div>                
              </div>
           
        </div>
    </div>
    </br>

        <h3> <em><p>Estamos en construcción, espéranos pronto</p></em><h3> <br><br><br><br>

</div>


<!-- Redes sociales Laterales
    ==========================================-->
    <div class="social2">   
         <ul>
             <li><a href="http://www.facebook.com/sharer.php?u=http://creinmustravel.com/prueba/agradecimiento.php&t=Creinmus" target="_blank" class="icon-facebook2" ></a></li>
             <li><a href="#" target="_blank" class="icon-google-plus"></a></li>
             <li><a href="#" target="_blank" class="icon-mail"></a></li>
             <li><a href="http://twitter.com/share" target="_blank" class="icon-twitter"></a></li>
    
        </ul>
    </div>
	

	<!-- Footer
    ==========================================-->
  

    <nav id="footer" style=" ;padding:100px 0;height:200px">
        <div class="container">
   
            <div class="text-center">
                <p>ALL RIGHTS RESERVED. COPYRIGHT © 2017.</p><p><a href="">Términos y condiciones</a></p> <p><a href="">Política de privacidad</a></p>
            </div>
      
        </div>
    </nav>
    


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/SmoothScroll.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.js"></script>

    <script src="js/owl.carousel.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>