-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-07-2017 a las 01:56:31
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registroexpeta`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrocreinmus`
--

CREATE TABLE `registrocreinmus` (
  `idRegistro` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `correo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registronuevemeses`
--

CREATE TABLE `registronuevemeses` (
  `idRegistro` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `correo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrotravel`
--

CREATE TABLE `registrotravel` (
  `idRegistro` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `correo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registrotravel`
--

INSERT INTO `registrotravel` (`idRegistro`, `nombre`, `correo`) VALUES
(1, 'jfjf', 'hd@gma'),
(15, 'hfhf', 'jdjd@gm'),
(57, 'dsds', 'dgdg@gmail.com'),
(58, 'fhhg', 'fjjg@jg'),
(59, 'nn', 'superadmin@gmil.com'),
(60, 'gjgj', 'superadmin@gmail.com'),
(61, 'jdhff', 'jchc@hghg'),
(62, 'ertertwerewr', 'werwer@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `registrocreinmus`
--
ALTER TABLE `registrocreinmus`
  ADD PRIMARY KEY (`idRegistro`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `registronuevemeses`
--
ALTER TABLE `registronuevemeses`
  ADD PRIMARY KEY (`idRegistro`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `registrotravel`
--
ALTER TABLE `registrotravel`
  ADD PRIMARY KEY (`idRegistro`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `registrocreinmus`
--
ALTER TABLE `registrocreinmus`
  MODIFY `idRegistro` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `registronuevemeses`
--
ALTER TABLE `registronuevemeses`
  MODIFY `idRegistro` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `registrotravel`
--
ALTER TABLE `registrotravel`
  MODIFY `idRegistro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
